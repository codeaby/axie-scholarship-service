const collectionsService = require('../services/collections.service');

const PREFIX = 'axiesch';

const scholarshipsCollection = collectionsService(`${PREFIX}_scholarships`);
const userScholarshipsCollection = collectionsService(`${PREFIX}_user_scholarships`);
const axiePricesCollection = collectionsService(`${PREFIX}_axie_prices`);

module.exports = {
  scholarshipsCollection,
  userScholarshipsCollection,
  axiePricesCollection,
};
