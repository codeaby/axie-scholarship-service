const joi = require('joi');
const admin = require('firebase-admin');
const {
  userScholarshipsCollection,
  scholarshipsCollection,
} = require('../collections/scholarship.collections');
const { getAxieInformation, getAxiePrice, getFullAxie } = require('../services/axies.service');
const { findUserScholarships } = require('../services/scholarships.service');
const { json } = require('express');

module.exports = {
  getUserScholarships: async (req, res) => {
    const { uid } = req.user;

    const scholarships = await findUserScholarships(uid);

    return res.json({ scholarships });
  },

  postScholarship: async (req, res) => {
    const { uid } = req.user;
    const { name, type } = req.body;

    const scholarship = await scholarshipsCollection.create({
      name,
      type,
      axies: [],
      creator: uid,
    });

    const userScholarships = await userScholarshipsCollection.find((ref) =>
      ref.where('user', '==', uid)
    );

    if (userScholarships) {
      userScholarships.scholarships.push({ id: scholarship.id, name, type });
      await userScholarshipsCollection.update(userScholarships.id, userScholarships);
    } else {
      await userScholarshipsCollection.create({
        scholarships: [{ id: scholarship.id, name, type }],
        user: uid,
      });
    }

    return res.status(201).json(scholarship);
  },

  postScholarshipSchema: joi.object({
    name: joi.string().required(),
    type: joi.string().valid('scholarship', 'buy-list').required(),
  }),

  getScholarship: async (req, res) => {
    const {
      user: { uid },
      params: { id },
    } = req;

    const scholarships = await findUserScholarships(uid);

    if (!scholarships.find((s) => s.id === id)) {
      return res.status(403).json({ message: 'Invalid Scholarship' });
    }

    const scholarship = await scholarshipsCollection.get(id);

    if (!scholarship) return res.status(404).json({ message: 'Scholarship not found' });

    scholarship.axies = await Promise.all(
      scholarship.axies.map(async (axie) => {
        const price = await getAxiePrice(axie);

        return { ...axie, price };
      })
    );

    return res.json(scholarship);
  },

  getPublicScholarship: async (req, res) => {
    const {
      params: { id },
    } = req;

    const scholarship = await scholarshipsCollection.get(id);

    if (!scholarship) return res.status(404).json({ message: 'Scholarship not found' });

    scholarship.axies = await Promise.all(
      scholarship.axies.map(async (axie) => {
        const price = await getAxiePrice(axie);

        return { ...axie, price };
      })
    );

    return res.json(scholarship);
  },

  postAddAxies: async (req, res) => {
    const {
      body: { axies },
      params: { id },
      user: { uid },
    } = req;

    const scholarships = await findUserScholarships(uid);

    if (!scholarships.find((s) => s.id === id)) {
      return res.status(403).json({ message: 'Invalid Scholarship' });
    }

    const scholarship = await scholarshipsCollection.get(id);

    if (!scholarship) return res.status(404).json({ message: 'Scholarship not found' });

    for (const axieId of axies.split(',')) {
      if (!scholarship.axies.includes(axieId)) {
        const axie = await getAxieInformation(axieId);

        if (axie) {
          scholarship.axies.push(axie);
        }
      }
    }
    try {
      await scholarshipsCollection.update(id, scholarship);
      return res.status(204).send();
    } catch (e) {
      return res.status(400).send({ message: 'Error saving' });
    }
  },

  postInviteScholarshipSchema: joi.object({
    email: joi.string().required(),
  }),

  postInviteScholarship: async (req, res) => {
    const {
      user: { uid },
      params: { id },
    } = req;
    const { email } = req.body;

    const scholarships = await findUserScholarships(uid);
    const scholarship = await scholarshipsCollection.get(id);

    if (!scholarships.find((s) => s.id === id)) {
      return res.status(403).json({ message: 'Invalid Scholarship' });
    }

    let invitedUser = null;

    try {
      invitedUser = await admin.auth().getUserByEmail(email);
    } catch (e) {
      // TODO: do nothing
    }

    if (!invitedUser) return res.status(400).json({ message: 'User not found' });

    const userScholarships = await userScholarshipsCollection.find((ref) =>
      ref.where('user', '==', invitedUser.uid)
    );

    if (userScholarships) {
      userScholarships.scholarships.push({ id: scholarship.id, name: scholarship.name });
      await userScholarshipsCollection.update(userScholarships.id, userScholarships);
    } else {
      await userScholarshipsCollection.create({
        scholarships: [{ id: scholarship.id, name: scholarship.name }],
        user: uid,
      });
    }

    return res.status(204).send();
  },

  deleteScholarshipFromUser: async (req, res) => {
    const {
      user: { uid },
      params: { id },
    } = req;

    const userScholarships = await userScholarshipsCollection.find((ref) =>
      ref.where('user', '==', uid)
    );

    if (!userScholarships) {
      return json.status(400).json({ message: 'User does not have scholarships' });
    }

    userScholarships.scholarships = userScholarships.scholarships.filter(
      (scholarship) => scholarship.id !== id
    );

    await userScholarshipsCollection.update(userScholarships.id, userScholarships);

    return res.status(204).send();
  },

  getAxieFromScholarship: async (req, res) => {
    const {
      user: { uid },
      params: { id, axie },
    } = req;

    const scholarships = await findUserScholarships(uid);

    if (!scholarships.find((s) => s.id === id)) {
      return res.status(403).json({ message: 'Invalid Scholarship' });
    }

    const scholarship = await scholarshipsCollection.get(id);

    if (!scholarship) return res.status(404).json({ message: 'Scholarship not found' });

    const scholarshipAxie = scholarship.axies.find((a) => a.id === axie);

    if (!scholarshipAxie) {
      return res.status(403).json({ message: 'Invalid Axie' });
    }

    const fullAxie = await getFullAxie(scholarshipAxie);

    return res.json(fullAxie);
  },

  deleteAxieFromScholarship: async (req, res) => {
    const {
      user: { uid },
      params: { id, axie },
    } = req;

    const scholarships = await findUserScholarships(uid);

    if (!scholarships.find((s) => s.id === id)) {
      return res.status(403).json({ message: 'Invalid Scholarship' });
    }

    const scholarship = await scholarshipsCollection.get(id);

    if (!scholarship) return res.status(404).json({ message: 'Scholarship not found' });

    scholarship.axies = scholarship.axies.filter((a) => a.id !== axie);

    await scholarshipsCollection.update(scholarship.id, scholarship);

    return res.status(204).send();
  },
};
