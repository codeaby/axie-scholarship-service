const getDocument = (doc) => ({ ...doc.data(), id: doc.id });
const getDocuments = (snapshot) => snapshot.docs.map(getDocument);

module.exports = {
  getDocument,
  getDocuments,
};
