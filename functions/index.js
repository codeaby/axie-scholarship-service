const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const express = require('express');
const cors = require('cors');
const validator = require('express-joi-validation').createValidator({});
const cookieParser = require('cookie-parser')();

// middlewares
const firebaseLoginMiddleware = require('./middlewares/firebase-login.middleware');
const { errorHandlerMiddleware, errorWrapper } = require('./middlewares/error-handler.middleware');

// controllers
const scholarshipsController = require('./controllers/scholarships.controller');

const api = express();

api.use(cors({ origin: true }));
api.use(express.json());
api.use(cookieParser);
api.use(firebaseLoginMiddleware);

// endpoints
api.get('/scholarships', errorWrapper(scholarshipsController.getUserScholarships));
api.post(
  '/scholarships',
  validator.body(scholarshipsController.postScholarshipSchema),
  errorWrapper(scholarshipsController.postScholarship)
);

api.get('/scholarships/:id', errorWrapper(scholarshipsController.getScholarship));
api.delete('/scholarships/:id', errorWrapper(scholarshipsController.deleteScholarshipFromUser));

api.post('/scholarships/:id/axies', errorWrapper(scholarshipsController.postAddAxies));

api.get(
  '/scholarships/:id/axies/:axie',
  errorWrapper(scholarshipsController.getAxieFromScholarship)
);
api.delete(
  '/scholarships/:id/axies/:axie',
  errorWrapper(scholarshipsController.deleteAxieFromScholarship)
);

api.post(
  '/scholarships/:id/invite',
  validator.body(scholarshipsController.postInviteScholarshipSchema),
  errorWrapper(scholarshipsController.postInviteScholarship)
);

// Public Endpoints
api.get('/public/scholarships/:id', errorWrapper(scholarshipsController.getPublicScholarship));

api.use(errorHandlerMiddleware);

exports.axiescholarship = functions.https.onRequest(api);
