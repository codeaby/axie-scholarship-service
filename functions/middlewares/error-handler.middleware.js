const errorHandlerMiddleware = (err, req, res, next) => {
  console.error(err);
  res.status(500).json({ message: 'Server side error' });
};

const errorWrapper = (controller) => async (req, res, next) => {
  try {
    await controller(req, res, next);
  } catch (e) {
    next(e);
  }
};

module.exports = {
  errorHandlerMiddleware,
  errorWrapper,
};
