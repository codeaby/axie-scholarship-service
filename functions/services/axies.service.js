const axios = require('axios');
const { request: gqlRequest, gql } = require('graphql-request');
const { axiePricesCollection } = require('../collections/scholarship.collections');

const ETH_PRICE_MULTIPLIER = 1000000000000000000;

const getAxieInformation = async (id) => {
  try {
    const {
      data: { class: axieClass, breedCount, parts, stats },
    } = await axios.get(`https://api.axie.technology/getgenes/${id}/all`);

    if (!axieClass) return null; // Excluding eggs

    const mappedParts = parts.reduce(
      (acum, part) => ({
        ...acum,
        [part.type.toLowerCase()]: part.id,
      }),
      {}
    );

    return {
      id,
      class: axieClass,
      breedCount,
      mappedParts,
      stats,
    };
  } catch (e) {
    console.error(e);
  }

  return null;
};

const getAvgPrice = (axies) => {
  const priceSum = axies.reduce(
    (sum, axie) => sum + parseFloat(axie.auction.currentPrice) / ETH_PRICE_MULTIPLIER,
    0
  );

  return parseFloat((priceSum / axies.length).toFixed(3));
};

const getAvgPriceUSD = (axies) => {
  const priceSum = axies.reduce((sum, axie) => sum + parseFloat(axie.auction.currentPriceUSD), 0);

  return parseFloat((priceSum / axies.length).toFixed(2));
};

const getAxiePriceGQL = async (axie) => {
  const query = gql`
    query GetAxieBriefList(
      $breedCount: Int!
      $class: AxieClass!
      $parts: [String!]
      $hp: Int!
      $speed: Int!
      $skill: Int!
      $morale: Int!
    ) {
      axies(
        auctionType: Sale
        criteria: {
          breedCount: [$breedCount, $breedCount]
          classes: [$class]
          parts: $parts
          hp: [$hp, $hp]
          speed: [$speed, $speed]
          skill: [$skill, $skill]
          morale: [$morale, $morale]
        }
        from: 0
        sort: PriceAsc
        size: 10
        owner: null
      ) {
        total
        results {
          auction {
            currentPrice
            currentPriceUSD
          }
        }
      }
    }
  `;

  try {
    const data = await gqlRequest('https://graphql-gateway.axieinfinity.com/graphql', query, {
      breedCount: axie.breedCount,
      class: axie.class,
      parts: [
        axie.mappedParts.mouth,
        axie.mappedParts.horn,
        axie.mappedParts.back,
        axie.mappedParts.tail,
      ],
      hp: axie.stats.hp,
      speed: axie.stats.speed,
      skill: axie.stats.skill,
      morale: axie.stats.morale,
    });

    const results = data.axies.total;
    let avgPrice = 0;
    let avgPriceUSD = 0;
    let minPrice = 0;
    let minPriceUSD = 0;

    if (results > 0) {
      avgPrice = getAvgPrice(data.axies.results);
      avgPriceUSD = getAvgPriceUSD(data.axies.results);

      minPrice = parseFloat(
        (parseFloat(data.axies.results[0].auction.currentPrice) / ETH_PRICE_MULTIPLIER).toFixed(3)
      );
      minPriceUSD = parseFloat(
        parseFloat(data.axies.results[0].auction.currentPriceUSD).toFixed(2)
      );
    }

    return {
      results,
      date: new Date(),
      avgPrice,
      avgPriceUSD,
      minPrice,
      minPriceUSD,
    };
  } catch (e) {
    console.log(e);
  }

  return null;
};

// older than an hour
const tooOld = (date) => new Date().getTime() - date.getTime() > 1 * 60 * 60 * 1000;

const updatePrice = async (axie, lastPrice) => {
  const gqlPrice = await getAxiePriceGQL(axie);

  if (gqlPrice) {
    await axiePricesCollection.create({
      class: axie.class,
      breedCount: axie.breedCount,
      back: axie.mappedParts.back,
      horn: axie.mappedParts.horn,
      mouth: axie.mappedParts.mouth,
      tail: axie.mappedParts.tail,
      hp: axie.stats.hp,
      speed: axie.stats.speed,
      skill: axie.stats.skill,
      morale: axie.stats.morale,
      ...gqlPrice,
    });

    return gqlPrice;
  } else {
    return lastPrice;
  }
};

const getAxiePrice = async (axie) => {
  const price = await axiePricesCollection.find((ref) =>
    ref
      .where('class', '==', axie.class)
      .where('breedCount', '==', axie.breedCount)
      .where('back', '==', axie.mappedParts.back)
      .where('horn', '==', axie.mappedParts.horn)
      .where('mouth', '==', axie.mappedParts.mouth)
      .where('tail', '==', axie.mappedParts.tail)
      .where('hp', '==', axie.stats.hp)
      .where('speed', '==', axie.stats.speed)
      .where('skill', '==', axie.stats.skill)
      .where('morale', '==', axie.stats.morale)
      .orderBy('date', 'desc')
      .limit(1)
  );

  if (price) {
    const { results, date, avgPrice, avgPriceUSD } = price;
    const lastPrice = { results, date: date.toDate(), avgPrice, avgPriceUSD };

    if (!tooOld(price.date.toDate())) {
      return lastPrice;
    } else {
      return updatePrice(axie, lastPrice);
    }
  }

  return updatePrice(axie, null);
};

const getFullAxie = async (axie) => {
  const price = await getAxiePrice(axie);

  const prices = await axiePricesCollection.getAll((ref) =>
    ref
      .where('class', '==', axie.class)
      .where('breedCount', '==', axie.breedCount)
      .where('back', '==', axie.mappedParts.back)
      .where('horn', '==', axie.mappedParts.horn)
      .where('mouth', '==', axie.mappedParts.mouth)
      .where('tail', '==', axie.mappedParts.tail)
      .where('hp', '==', axie.stats.hp)
      .where('speed', '==', axie.stats.speed)
      .where('skill', '==', axie.stats.skill)
      .where('morale', '==', axie.stats.morale)
      .orderBy('date', 'desc')
      .limit(48)
  );

  return {
    ...axie,
    price,
    prices: prices.map(({ date, avgPrice, avgPriceUSD }) => ({
      date: date.toDate(),
      avgPrice,
      avgPriceUSD,
    })),
  };
};

module.exports = {
  getAxieInformation,
  getAxiePrice,
  getFullAxie,
};
