const db = require('../database/database');
const { getDocument, getDocuments } = require('../database/get-documents');

const collectionsService = (collectionName) => {
  const collection = db.collection(collectionName);

  const getAll = async (filters = (ref) => ref) => getDocuments(await filters(collection).get());

  const create = async (document) => getDocument(await (await collection.add(document)).get());

  const get = async (id) => getDocument(await collection.doc(id).get());

  const exists = async (id) => (await collection.doc(id).get()).exists;

  const update = async (id, document) => await collection.doc(id).update(document);

  const find = async (filters = (ref) => ref) => {
    const documents = getDocuments(await filters(collection).get());
    return !documents.length ? null : documents[0];
  };

  return {
    getAll,
    create,
    get,
    exists,
    update,
    find,
  };
};

module.exports = collectionsService;
