const { userScholarshipsCollection } = require('../collections/scholarship.collections');

const findUserScholarships = async (uid) => {
  const userScholarships = await userScholarshipsCollection.find((ref) =>
    ref.where('user', '==', uid)
  );

  return userScholarships ? userScholarships.scholarships : [];
};

module.exports = {
  findUserScholarships,
};
